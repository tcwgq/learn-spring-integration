package com.tcwgq.mapper.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.tcwgq.entity.User;
import com.tcwgq.mapper.UserMapper;

/**
 * @Author tcwgq
 * @Date 2018年10月14日 下午1:46:53
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class UserMapperTest {
	@Autowired
	private UserMapper userMapper;

	@Test
	public void test() {
		User user = userMapper.getByUsername("zhangsan");
		System.out.println(JSON.toJSONString(user));
	}
}
