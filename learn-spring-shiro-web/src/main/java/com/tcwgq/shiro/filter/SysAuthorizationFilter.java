package com.tcwgq.shiro.filter;

import com.alibaba.fastjson.JSON;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * shiro默认的拦截是要满足所有的条件，但有时我们只要满足其中的一个，用于拥有perm1或perm2任何一个条件就可以访问/doadd.html。这时我们可以重写过滤器，将and变成or
 * <p>
 * Created by tcwgq on 2018/10/14 17:48.
 */
public class SysAuthorizationFilter extends AuthorizationFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(SysAuthorizationFilter.class);

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        Subject subject = getSubject(servletRequest, servletResponse);
        String[] permissions = (String[]) o;
        LOGGER.info("permissions {}", JSON.toJSONString(permissions));
        // 没有权限限制
        if (permissions == null || permissions.length == 0) {
            return true;
        }
        // 只要有一个权限就放行
        for (String perm : permissions) {
            if (subject.isPermitted(perm)) {
                return true;
            }
        }
        return false;
    }
}
