package com.tcwgq.shiro.session;

import com.tcwgq.utils.SerializableUtil;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.ValidatingSession;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tcwgq on 2018/10/14 16:35.
 */
public class CustomSessionDao extends CachingSessionDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //在创建完session之后会调用
    @Override
    protected Serializable doCreate(Session session) {
        System.out.println("doCreate");
        Serializable sessionId = generateSessionId(session);
        assignSessionId(session, sessionId);
        String sql = "insert into sessions(id, session) values(?,?)";
        jdbcTemplate.update(sql, sessionId, SerializableUtil.serialize(session));
        return session.getId();
    }

    //更新session最后访问时间、停止session、设置超时时间、移除属性值时会调用
    @Override
    protected void doUpdate(Session session) {
        System.out.println("doUpdate");
        if (session instanceof ValidatingSession && !((ValidatingSession) session).isValid()) {
            return; //如果会话过期/停止 没必要再更新了
        }
        String sql = "update sessions set session=? where id=?";
        jdbcTemplate.update(sql, SerializableUtil.serialize(session), session.getId());
    }

    //用户logout、session过期会调用
    @Override
    protected void doDelete(Session session) {
        System.out.println("doDelete");
        String sql = "delete from sessions where id=?";
        jdbcTemplate.update(sql, session.getId());
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        String sql = "select session from sessions where id=?";
        List<String> sessionStrList = jdbcTemplate.queryForList(sql, String.class, sessionId);
        if (sessionStrList.size() == 0) return null;
        return SerializableUtil.deserialize(sessionStrList.get(0));
    }

}
