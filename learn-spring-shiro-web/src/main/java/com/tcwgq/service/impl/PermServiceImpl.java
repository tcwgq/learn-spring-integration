package com.tcwgq.service.impl;

import com.tcwgq.entity.Perm;
import com.tcwgq.mapper.PermMapper;
import com.tcwgq.service.PermService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
@Service
public class PermServiceImpl extends ServiceImpl<PermMapper, Perm> implements PermService {
    @Autowired
    private PermMapper permMapper;

    @Override
    public List<Perm> findByRoleId(Integer roleId) {
        return permMapper.findByRoleId(roleId);
    }
}
