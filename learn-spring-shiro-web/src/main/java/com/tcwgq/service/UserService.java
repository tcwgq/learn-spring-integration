package com.tcwgq.service;

import com.tcwgq.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
public interface UserService extends IService<User> {
    public User getUser(String username);
}
