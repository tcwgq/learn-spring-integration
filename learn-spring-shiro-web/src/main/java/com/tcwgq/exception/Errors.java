package com.tcwgq.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tcwgq on 2018/10/2 13:25.
 */
public enum Errors implements ErrorCode {
    ERRORS_SYSTEM(11111, "系统错误");
    private Integer code;
    private String msg;

    private static final Map<Integer, Errors> CACHE = new HashMap<>();

    static {
        for (Errors errors : Errors.values()) {
            CACHE.put(errors.getCode(), errors);
        }
    }

    Errors(Integer code, String msg) {
        this.code = 1002500000 + code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    public Errors gerErrors(Integer code) {
        return CACHE.get(code);
    }
}
