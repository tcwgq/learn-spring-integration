package com.tcwgq.controller;

import com.tcwgq.service.ShiroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by tcwgq on 2018/10/14 12:00.
 */
@Controller
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private ShiroService shiroService;

    @RequestMapping(value = {"", "/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public String doLogin(String username, String password, ModelMap map) {
        try {
            shiroService.doLogin(username, password);
        } catch (Exception e) {
            map.put("msg", e.getMessage());
            return "error";
        }
        return "index";
    }
}
