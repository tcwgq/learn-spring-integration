package com.tcwgq.controller;


import com.tcwgq.entity.User;
import com.tcwgq.response.ApiResponse;
import com.tcwgq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/get", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResponse<User> get(Integer id) {
        return ApiResponse.success(userService.selectById(id));
    }
}

