package com.tcwgq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tcwgq.model.User;

@Controller
@RequestMapping("/redis")
public class RedisController {
	@Autowired
	private RedisTemplate<String, User> template;

	@ResponseBody
	@RequestMapping(value = "/save", method = { RequestMethod.GET, RequestMethod.POST })
	public String save(String name, Integer age) {
		if (age == null) {
			age = 23;
		}
		User user = new User(name, age);
		template.opsForValue().set(name, user);
		return "OK";
	}

	@ResponseBody
	@RequestMapping(value = "/get", method = { RequestMethod.GET, RequestMethod.POST })
	public User get(String name) {
		User user = template.opsForValue().get(name);
		return user;
	}

	@ResponseBody
	@RequestMapping(value = "/update", method = { RequestMethod.GET, RequestMethod.POST })
	public User update(String name) {
		User user = template.execute(new RedisCallback<User>() {
			@Override
			public User doInRedis(RedisConnection connection) throws DataAccessException {
				connection.set(name.getBytes(), name.getBytes());
				return null;
			}
		});
		return user;
	}

}
