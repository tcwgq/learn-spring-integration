package com.tcwgq.dao;

import com.tcwgq.domain.User;

import java.util.List;

public interface UserDao {
    void save(User user);

    void update(User user);

    void delete(Integer id);

    User getUser(Integer id);

    List<User> getAllUser();
}
