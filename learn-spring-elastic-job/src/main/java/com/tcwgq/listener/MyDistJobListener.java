package com.tcwgq.listener;

import com.dangdang.ddframe.job.executor.ShardingContexts;
import com.dangdang.ddframe.job.lite.api.listener.AbstractDistributeOnceElasticJobListener;
import lombok.extern.slf4j.Slf4j;

/**
 * @author iyb-wangguangqiang 2019/8/2 11:46
 */
@Slf4j
public class MyDistJobListener extends AbstractDistributeOnceElasticJobListener {
    public MyDistJobListener(long startedTimeoutMilliseconds, long completedTimeoutMilliseconds) {
        super(startedTimeoutMilliseconds, completedTimeoutMilliseconds);
    }

    @Override
    public void doBeforeJobExecutedAtLastStarted(ShardingContexts shardingContexts) {
        log.info("doBeforeJobExecutedAtLastStarted...");
    }

    @Override
    public void doAfterJobExecutedAtLastCompleted(ShardingContexts shardingContexts) {
        log.info("doAfterJobExecutedAtLastCompleted...");
    }
}
