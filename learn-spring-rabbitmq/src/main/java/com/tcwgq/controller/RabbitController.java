package com.tcwgq.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/rabbit")
public class RabbitController {

	@Autowired
	private RabbitTemplate template;

	@ResponseBody
	@RequestMapping(value = "/send", method = { RequestMethod.GET, RequestMethod.POST })
	public String send(String msg) {
		template.convertAndSend("ab", msg);
		return msg;
	}

}
